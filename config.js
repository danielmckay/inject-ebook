// Global configuration variables
var config = {
  'homepage': 'http://my-test.hrw.com/content/hmof/language_arts/hmhcollections/na/gr8/ese_9780544088207_/index.html',
  'injectCssFile': 'inject.css',
  'injectJsFile': 'inject-script-ebook.js',
  'authUrl': 'http://my-test.hrw.com/api/identity/v1/authorize;contextId=hmof?response_type=code&scope=openid+profile&client_id=HMHPlayer+Id&state=HMHPlayerRedirect%7B%7Bhttp%3A%2F%2Fmy-test.hrw.com%2Fcontent%2Fhmof%2Fmath%2Fgomath%2Fca%2Fgr8%2Fonline_interactive_teacher_book_9780544268425_%2FG8_U1_M1_L1%2Flaunch.html%3Fplayer%3Dgoplayer%26edition%3DStudent%7D%7D&redirect_uri=http%3A%2F%2Fmy-test.hrw.com%2FHMHPlayer_AuthCallback',
  'sifTokenUrl': 'http://my-test.hrw.com/api/identity/v1/token;contextId=hmof', 
  'username': 'Ent9477@6y',
  'password': 'Ent9477@6y'
};

//'authUrl': 'https://my-test.hrw.com/api/identity/v1/authorize;contextId=hmof?response_type=code&scope=openid+profile&client_id=HMHPlayer+Id&state=HMHPlayerRedirect%7B%7Bhttps%3A%2F%2Fmy-test.hrw.com%2Fcontent%2Fhmof%2Fmath%2Fhsm%2Fcommon%2Fite%2Fn01.4%2Findex.html%3FC%3Dalg1%26S%3DCA%7D%7D&redirect_uri=https%3A%2F%2Fmy-test.hrw.com%2FHMHPlayer_AuthCallback',


// 'authUrl': 'https://my-review-cert.hrw.com/api/identity/v1/authorize;contextId=hmof?response_type=code&scope=openid+profile&client_id=HMHPlayer+Id&state=HMHPlayerRedirect%7B%7Bhttps%3A%2F%2Fmy-review-cert.hrw.com%2Fcontent%2Fhmof%2Fmath%2Fhsm%2Fcommon%2Fite%2Fn01.4%2Findex.html%3FC%3Dalg1%26S%3DCA%7D%7D&redirect_uri=https%3A%2F%2Fmy-review-cert.hrw.com%2FHMHPlayer_AuthCallback',