inject-ebook
============

This is a proof of concept application to illustrate the ability of a Chrome App
to inject CSS stylesheets and JavaScript code into a piece of embedded
external content via a webview.

Further information on how it is done is available on this [Confluence page](http://dubconf.hmhpub.com:8080/display/~mckayd/Tech-Spike+eBook+Shell+Preparation+-+Script+Injection+and+CSS)

To install:

- Download the source code (clone the repo)
- Go to chrome://extensions/ in your Chrome browser
- Drag and drop the inject-ebook root folder into the browser window 
- Launch 
