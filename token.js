var token = (function(configModule) {
  var Token = function(
      sifTokenUrl,          // URL to load in webview
      username,             // username for platform
      password,             // username for platform
      codePanel,            // Placeholder for CSS mark up
      button) {             // Input (textarea) node for CSS
    this.sifTokenUrl = sifTokenUrl;
    this.username = username;
    this.password = password;
    this.codePanel = codePanel;
    this.button = button;
    
    this.init();
  };

  Token.prototype.init = function() {
    var that = this;
    this.button.addEventListener('click', function(e) {
      console.log('button clicked');
      that.getSifToken();
    });
  };

  Token.prototype.getSifToken = function(callback) {
    var that = this;
    var request = new XMLHttpRequest();
    request.open('POST', this.sifTokenUrl, true);
    request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    request.onload = function() {
      console.log('onload FIRED!!!');
      if (request.status >= 200 && request.status < 400) {
        // Success!
        var resp = JSON.parse(request.responseText);
        configModule.sifToken = resp.access_token;
        that.codePanel.innerText = configModule.sifToken;
      } else {
        // We reached our target server, but it returned an error
        console.log('Server error');
      }
    };
    request.onerror = function() {
      // There was a connection error of some sort
      console.log('Connection error');
    };
    request.send(this.buildSifRequestBody());
  };

  Token.prototype.buildSifRequestBody = function() {
    return 'username=' + encodeURIComponent(this.username) + 
           '&password=' + encodeURIComponent(this.password) + 
           '&grant_type=password';
  };


  return {'Token': Token};
}(config));