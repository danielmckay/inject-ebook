var mainCss = null;
var mainInjectJs = null;
var mainCookie = null;
var mainToken = null;
(function(configModule, cssModule, injectJsModule, cookieModule, tokenModule) {
  var query = function(str) { return document.querySelector(str); };

  window.addEventListener('load', function(e) {
    mainCss = new cssModule.Css(
        configModule.homepage,
        configModule.injectCssFile,
        query('#contentWebview'),
        query('#cssMarkUp'),
        query('#injectCssBtn'));
    mainInjectJs = new injectJsModule.InjectJs(
        configModule.homepage,
        configModule.injectJsFile,
        query('#contentWebview'),
        query('#cookieCode'),
        query('#injectCookieBtn'));
    mainCookie = new cookieModule.Cookie(
        configModule.authUrl,
        query('#cookieWebview'),
        query('#authCookieCode'),
        query('#getCookieBtn')
      );
    mainToken = new tokenModule.Token(
        configModule.sifTokenUrl,
        configModule.username,
        configModule.password,
        query('#tokenCode'),
        query('#getTokenBtn')
      );
    });
}(config, css, injectJs, cookie, token));