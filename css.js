var css = (function(configModule) {
  var Css = function(
      homepage,         // URL to load in webview
      injectCssFile,    // Name of file from which to inject the css
      webview,          // Webview DOM node
      codePanel,        // Placeholder for CSS mark up
      button) {         // Input (textarea) node for CSS
    this.homepage = homepage;
    this.injectCssFile = injectCssFile;
    this.webview = webview;
    this.codePanel = codePanel;
    this.button = button;

    this.hasInit = false;     // Flag so that we don't load css initially
    this.loadOnStop = false;
    this.cssString = '';

    this.init();
  };

  Css.prototype.init = function() {
    // Load "homepage"
    console.log('this', this)
    this.webview.src = this.homepage;

    (function(css) {
      // Hook up CSS injection for each page load
      css.webview.addEventListener('loadcommit', function(e) { 
        return css.doLoadCommit(e);
      });
      css.webview.addEventListener('loadstop', function(e) { 
        return css.doLoadStop(e); 
      });
      // Update state and reload when committing to new URL pattern and CSS
      css.button.addEventListener('click', function(e) {
        e.preventDefault();
        css.hasInit = true;
        css.webview.reload();
      });

      // Fetch initial CSS file
      (function(xhr) {
        xhr.addEventListener('readystatechange', function(e) {
          if (xhr.readyState == 4) {
            css.cssString = xhr.responseText;
            css.codePanel.innerText = css.cssString;
          }
        });
        xhr.open('GET', css.injectCssFile, true);
        xhr.send();
      }(new XMLHttpRequest()));

    }(this));
  };

  Css.prototype.doLoadCommit = function(e) {
    if (e.url !== null) {
      this.loadOnStop = true;
    }
  };

  Css.prototype.doLoadStop = function(e) {
    if (this.loadOnStop) {
      if(this.hasInit) {
        this.injectCssFromFile();
      }
      this.loadOnStop = false;
    }
  };

  Css.prototype.injectCssFromFile = function() {
    this.webview.insertCSS({'file': this.injectCssFile});
  };

  return {'Css': Css};
}(config));