var injectJs = (function(configModule) {
  var InjectJs = function(
      homepage,         // URL to load in webview
      fileToInject,     // Name of file from which to inject the css
      webview,          // Webview DOM node
      codePanel,        // Placeholder for CSS mark up
      button) {         // Input (textarea) node for CSS
    this.homepage = homepage;
    this.fileToInject = fileToInject;
    this.webview = webview;
    this.codePanel = codePanel;
    this.button = button;

    this.hasInit = false;     // Flag so that we don't load css initially
    this.loadOnStop = false;
    this.init();
  };

  InjectJs.prototype.init = function() {
    this.webview.src = this.homepage;

    (function(injector) {
      // Add event listeners to UI elements      
      injector.webview.addEventListener('loadcommit', function(e) {
        return injector.doLoadCommit(e);
      });
      injector.webview.addEventListener('loadstop', function(e) { 
        return injector.doLoadStop(e); 
      });
      injector.button.addEventListener('click', function(e) {
        e.preventDefault();
        injector.hasInit = true;
        injector.webview.reload();
      });
    }(this));
  };

  InjectJs.prototype.doLoadCommit = function(e) {
    if (e.url !== null) {
      this.loadOnStop = true;
    }
  };

  InjectJs.prototype.doLoadStop = function(e) {
    if (this.loadOnStop) {
      if(this.hasInit) {
        this.injectJsFromFile();
      }
      this.loadOnStop = false;
    }
  };

  InjectJs.prototype.injectJsFromFile = function() {
    console.log('this.fileToInject', this.fileToInject); 
    this.webview.executeScript({ file: this.fileToInject }, function(results) {
      // results[0] would have the webview's innerHTML.
      console.log('results', results);
    });
  };

  return {'InjectJs': InjectJs};
}(config));