var cookie = (function(configModule) {
  var Cookie = function(
      authUrl,          // URL to load in webview
      webview,          // Webview DOM node
      codePanel,        // Placeholder for CSS mark up
      button) {         // Input (textarea) node for CSS
    this.authUrl = authUrl;
    this.webview = webview;
    this.codePanel = codePanel;
    this.button = button;
    this.hasInit = false;
    this.responseHeaders = [];

    this.init();
  };

  Cookie.prototype.init = function() {
    // this.webview.src = this.authUrl;
    var that = this;

    this.webview.request.onBeforeSendHeaders.addListener(function(data) {
      if(configModule.sifToken) {
        data.requestHeaders.push({ name: "Authorization", value: configModule.sifToken });
        data.requestHeaders.push({ name: "Content-Type", value: "application/x-www-form-urlencoded" });
      }
      return {
        requestHeaders: data.requestHeaders
      }
    },
    { urls: ['*://*/*'] },
    ['blocking', 'requestHeaders']);

    this.webview.request.onHeadersReceived.addListener(function(details) {
      // get the cookie from the SETCOOKIE header
      console.log(details);
    },
    { urls: ['*://*/*'] }, 
    ['blocking', 'responseHeaders']);

    this.button.addEventListener('click', function(e) {
      if(that.hasInit) {
        that.webview.reload();
      } else {
        that.webview.src = that.authUrl;
        that.hasInit = true;
      }
      
    });

  };

  return {'Cookie': Cookie};
}(config));